<br/>


![Image of Yaktocat](https://jax.network/wp-content/uploads/2020/10/logo.png)  



# Miner

This repository contains source code of the PoC implementation of the JaxNetwork miner.
It supports mining of the Beacon chain as well as Shard chain(s). 
Current implementation also supports network monitoring and automatically picks newly created shards for mining 
(automatic reaction to the network expansion). 

<br/>

## How to build
* Use [Dockerfile](https://gitlab.com/jaxnet/core/miner/-/blob/master/Dockerfile) to build and run docker image, or
* clone this repo and just `go build -o /miner`

<br/>

## How to run
* Ensure `config.yaml` is locate in _the same directory_ where is the miner's executable located. 
You can find the [example of the configuration file here](https://gitlab.com/jaxnet/core/miner/-/blob/master/conf.yaml).  
* Follow the parameter's description provided in this file to adjust settings corresponding to your infrastructure configuration.
* Launch the executable: `./miner`
* In case of successful activation - miner would begin sending blocks to the blockchain node after few minutes. You can follow this proccess using the `operations.log` file. 

#### Important

1. _Please, note: current implementation does not provide any failover strategy. Use external tools (like `systemd`) to keep it up and running during possible failures._  
2. _In case if beacon chain is not mining - any shard chains would **NOT** be mined as well._

<br/>

## Issues
In case if provided implementation does not work as expected it is recommended to switch to the `debug` mode in `conf.yaml`.
In this mode the executable would provide much more context about what is going on and why things are not going as expected.
Please, not hesitate to open issues in this repository with the description of the problem you have encountered into. 
Please, do not forget to attach `operations.log` file after checking for the sensitive info (e.g. IP addresses of the internal resources).   

<br/>

## License
ISC. Please see the LICENSE file for the details.

