module gitlab.com/jaxnet/core/miner

go 1.15

require (
	github.com/btcsuite/btcd v0.22.0-beta
	github.com/btcsuite/btcutil v1.0.3-0.20201208143702-a53e38424cce
	github.com/google/uuid v1.2.0
	github.com/rs/zerolog v1.23.0
	gitlab.com/jaxnet/core/merged-mining-tree v1.0.5
	gitlab.com/jaxnet/jaxnetd v0.3.3-beta
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)

//replace gitlab.com/jaxnet/jaxnetd v0.3.2-beta => ../jaxnetd
