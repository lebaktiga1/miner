/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package communicator

import (
	"fmt"
	"time"

	btcdrpcclient "github.com/btcsuite/btcd/rpcclient"
	btcdutil "github.com/btcsuite/btcutil"
	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/core/miner/core/common"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/network/rpcclient"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

const (
	connectionErrorNextAttemptTimeout = time.Second * 5
)

// [async]
//
// processMinedBitcoinBlocks creates RPC-connection to the blockchain node
// and begins sending of the mined blocks that arrive from the miner back to the node.
// Internally checks connection's state and in case of any error - recreates it from scratch.
func (c *Communicator) processMinedBitcoinBlocks() {

	tryEstablishRPCConn := func(conf *btcdrpcclient.ConnConfig) (client *btcdrpcclient.Client) {
		var err error
		const logContext = "btcd"
		for {
			if c.config.Debug {
				c.senderLogInfo().Str(
					"ctx", logContext).Str(
					"host", conf.Host).Msg("Connecting to the node")
			}

			client, err = btcdrpcclient.New(conf, nil)
			if err == nil {
				c.senderLogInfo().Str(
					"ctx", logContext).Str(
					"host", conf.Host).Msg("Connection established")

				return
			}

			if c.config.Debug {
				c.senderLogInfo().Str(
					"ctx", logContext).Str(
					"host", conf.Host).Msg("Can't connect to the node")
			}

			time.Sleep(connectionErrorNextAttemptTimeout)
		}
	}

	go func() {
		var (
			client *btcdrpcclient.Client
			err    error
		)

		for {
			conf := c.config.BitcoinRPCConf()
			client = tryEstablishRPCConn(conf)

			for {
				if c.MustBeStopped {
					c.senderLogTrace().Msg("Stopped Bitcoin blocks fetching goroutine")
					return
				}

				select {
				case block := <-c.outgoingBitcoinBlocks:
					wireBlock := btcdutil.NewBlock(&block)

					err = client.SubmitBlock(wireBlock, nil)
					if err != nil {
						c.senderLogErr(err).
							Stringer("hash", wireBlock.MsgBlock().BlockHash()).
							Msg("Can't send mined Bitcoin block to the node")
						break
					}

					if c.config.Debug {
						c.senderLogTrace().
							Int32("height", wireBlock.Height()).
							Stringer("hash", wireBlock.MsgBlock().BlockHash()).
							Msg("Bitcoin block sent")
					}
				}
			}
		}
	}()
}

// [async]
//
// processMinedBeaconBlocks creates RPC-connection to the blockchain node
// and begins sending of the mined blocks that arrive from the miner back to the node.
// Internally checks connection's state and in case of any error - recreates it from scratch.
func (c *Communicator) processMinedBeaconBlocks() {
	go func() {
		var (
			client *rpcclient.Client
			err    error
		)

		for {
			conf := c.config.BeaconRPCConf()
			client = c.tryEstablishRPCConnectionUntilSuccess(conf, "beacon")

			for {
				if c.MustBeStopped {
					c.senderLogTrace().Msg("Stopped beacon blocks fetching goroutine")
					return
				}

				select {
				case block := <-c.outgoingBeaconBlocks:
					wireBlock := jaxutil.NewBlock(&block)

					err = client.ForBeacon().SubmitBlock(wireBlock, nil)
					if err != nil {
						c.senderLogErr(err).
							Stringer("hash", wireBlock.MsgBlock().BlockHash()).
							Stringer("pow_hash", wireBlock.MsgBlock().Header.PoWHash()).
							Msg("Cant send mined beacon block to the node")
						break
					}

					if c.config.Debug {
						c.senderLogTrace().
							Int32("height", wireBlock.Height()).
							Stringer("hash", wireBlock.MsgBlock().BlockHash()).
							Stringer("pow_hash", wireBlock.MsgBlock().Header.PoWHash()).
							Msg("Beacon block sent")
					}
				}
			}
		}
	}()
}

// [async]
//
// processMinedShardsBlocks creates RPC-connection to the blockchain node (connection per shard)
// and begins sending of the mined shard blocks that arrive from the miner back to the node.
// Internally checks connection's state and in case of any error - recreates it from scratch.
func (c *Communicator) processMinedShardsBlocks() {
	for shardID, channel := range c.outgoingShardsBlocksChannels {
		go func(ch chan *wire.MsgBlock, sid common.ShardID) {
			var (
				conf   *rpcclient.ConnConfig
				client *rpcclient.Client
				err    error
			)

			for {
				conf, err = c.config.ShardRPCConf(sid)
				if err != nil {
					// This is unrecoverable error.
					// The process must be stopped here due to the incomplete config file.
					err = fmt.Errorf("incomplete miner configuration occured: "+
						"not enough info about shards RPC: %v", err)
					panic(err)
				}

				client = c.tryEstablishRPCConnectionUntilSuccess(conf, fmt.Sprint("shard/", sid))

				for {
					if c.MustBeStopped {
						return
					}

					select {
					case block := <-ch:
						{
							wireBlock := jaxutil.NewBlock(block)

							// WARN: [type overflow]
							//       Potential type overflow here in case if "shardID" would change its base type.
							err = client.ForShard(uint32(sid)).SubmitBlock(wireBlock, nil)
							if err != nil {
								c.senderLogErr(err).Msg("Cant send mined shard block to the blockchain node")
								break
							}

							if c.config.Debug {
								// This log calculates block header hash, that is a bit heavy operation.
								// That's why it is wrapped to log only in debug mode.
								c.senderLogInfo().
									Uint32("ShardID", uint32(sid)).
									Int32("height", wireBlock.Height()).
									Stringer("hash", wireBlock.MsgBlock().BlockHash()).
									Stringer("pow_hash", wireBlock.MsgBlock().Header.PoWHash()).
									Msg("Shard block sent")
							}

						}
					}
				}
			}
		}(channel, shardID)
	}
}

// tryEstablishRPCConnectionUntilSuccess tries to establish RPC connection are return it's handler.
// In case of any error - would wait for some time and would try again.
// Hangs until connection would be established well.
func (c *Communicator) tryEstablishRPCConnectionUntilSuccess(conf *rpcclient.ConnConfig, logContext string) (client *rpcclient.Client) {
	var err error

	for {
		if c.config.Debug {
			c.senderLogInfo().Str(
				"ctx", logContext).Str(
				"host", conf.Host).Msg("Connecting to the node")
		}

		client, err = rpcclient.New(conf, nil)
		if err == nil {
			c.senderLogInfo().Str(
				"ctx", logContext).Str(
				"host", conf.Host).Msg("Connection established")

			return
		}

		if c.config.Debug {
			c.senderLogInfo().Str(
				"ctx", logContext).Str(
				"host", conf.Host).Msg("Can't connect to the node")
		}

		time.Sleep(connectionErrorNextAttemptTimeout)
	}
}

func (c *Communicator) senderLogInfo() *zerolog.Event {
	return logger.Log.Info().Str("component", "communicator/sender")
}

func (c *Communicator) senderLogTrace() *zerolog.Event {
	return logger.Log.Trace().Str("component", "communicator/sender")
}

func (c *Communicator) senderLogErr(err error) *zerolog.Event {
	return logger.Log.Err(err).Str("component", "communicator/sender")
}
