/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package events

import (
	btcdjson "github.com/btcsuite/btcd/btcjson"

	"gitlab.com/jaxnet/core/miner/core/common"
	"gitlab.com/jaxnet/jaxnetd/types/jaxjson"
)

type ShardBlockCandidate struct {
	ShardID   common.ShardID
	Candidate *jaxjson.GetShardBlockTemplateResult
}

type BeaconBlockCandidate struct {
	Candidate *jaxjson.GetBeaconBlockTemplateResult
}

type BitcoinBlockCandidate struct {
	Candidate *btcdjson.GetBlockTemplateResult
}
