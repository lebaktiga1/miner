/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package miner

import (
	"time"

	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
)

// Represents the mining process state.
// It is used for storing and restoring of the context of the miner between mining rounds.
type context struct {
	task       *tasks.MinerTask
	result     tasks.MinerResult
	ticker     *time.Ticker
	nonce      uint32
	extraNonce uint64
}
