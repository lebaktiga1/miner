/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package miner

import (
	"bytes"
	"fmt"
	"math"
	"math/big"
	"sync"
	"time"

	"github.com/btcsuite/btcd/blockchain"
	btcdwire "github.com/btcsuite/btcd/wire"
	btcdutil "github.com/btcsuite/btcutil"
	"github.com/rs/zerolog"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/state"
	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/node/encoder"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/pow"
)

const (
	// CoinbaseFlags is added to the coinbase script of a generated block
	// and is used to monitor BIP16 support as well as blocks that are
	// generated via btcd.
	CoinbaseFlags = "/P2SH/jaxnet/"

	// MiningRoundTimeWindow defines how much time would be taken by one miming round.
	// Mining round - it is a calculations cycle, during which miner do not accepts new tasks
	// (does not react on changes in blocks configuration).
	// During this time window the miner is trying to solve the PoC for beacon/shards(s) chain blocks,
	// that has been received on a last update.
	MiningRoundTimeWindow = time.Millisecond * 150
)

type Miner struct {
	utils.StoppableMixin
	miningStopped bool

	// Collects solved blocks (mining results) for further transfer via RPC.
	results chan tasks.MinerResult

	config *settings.Configuration
}

func New(conf *settings.Configuration) (solver *Miner) {
	// By default results chan is expected to be non-blocking (buffered channel should be used),
	// to not affect miner with hangs/lags of the RPC interface.
	const kResultsCapacity = 256
	solver = &Miner{
		results: make(chan tasks.MinerResult, kResultsCapacity),
		config:  conf,
	}

	solver.IsStopped = func() bool {
		return solver.miningStopped
	}

	return
}

func (m *Miner) RunUsing(cache *state.Coordinator) {
	var (
		ctx      *context
		nextTask *tasks.MinerTask
	)

	for {
		if m.MustBeStopped {
			m.miningStopped = true
			return
		}

		nextTask = cache.NextTask()
		if nextTask == nil && ctx == nil {
			// There is no work available for miner.
			// Even if current task is not nil - no mining must be done,
			// because the actual BC header has been set to nil on the ctx's side.
			//
			// Wait some time and check again later.
			time.Sleep(MiningRoundTimeWindow)
			continue
		}

		if ctx == nil || nextTask != ctx.task {
			// The task has been updated, so the mining context must be reset.
			// In all other cases the mining would be continued with previous context.
			extraNonceOffset, _ := encoder.RandomUint64()
			ctx = &context{
				task:       nextTask,
				result:     tasks.NewMinerResult(),
				ticker:     time.NewTicker(MiningRoundTimeWindow),
				extraNonce: extraNonceOffset,
			}

			// Setting timestamps for all blocks in task.
			timestamp := time.Now()

			if m.config.EnableBTCMining {
				ctx.task.BitcoinBlock.Header.Timestamp = timestamp
			}

			ctx.task.BeaconBlock.Header.SetTimestamp(timestamp)
			for _, shard := range ctx.task.ShardsTargets {
				shard.BlockCandidate.Header.SetTimestamp(timestamp)
			}

			err := m.UpdateShardsMerkleRoot(ctx)
			if err != nil {
				m.logErr(err)
				continue
			}

			for _, shard := range ctx.task.ShardsTargets {
				shard.BlockCandidate.Header.SetTimestamp(timestamp)
			}
		}

		if m.config.EnableBTCMining {
			m.solveUsingBtc(ctx)
		} else {
			m.solveUsingBeacon(ctx)
		}

		if ctx.result.ContainsMinedBlocks(m.config.EnableBTCMining) {
			m.results <- ctx.result
			ctx = nil
		}
	}
}

func (m *Miner) StopUsing(wg *sync.WaitGroup) {
	close(m.results)
	m.StoppableMixin.StopUsing(wg)
}

// Results returns channel to send miner result.
// Changed from read only channel, possibly bad decision
func (m *Miner) Results() chan tasks.MinerResult {
	return m.results
}

func (m *Miner) solveUsingBtc(state *context) {
	atLeastOneBlockMined := false
	btcBlock := state.task.BitcoinBlock

	for ; state.extraNonce < math.MaxUint64; state.extraNonce++ {
		// Update the extra nonce in the block template with the
		// new value by regenerating the coinbase script and
		// setting the merkle root to the new value.
		err := m.UpdateBitcoinExtraNonce(state, btcBlock)
		if err != nil {
			m.logErr(err)
			break
		}

		// Search through the entire nonce range for a solution while
		// periodically checking for early quit and stale block
		// conditions along with updates to the speed monitor.
		for ; state.nonce < math.MaxUint32; state.nonce++ {
			select {
			case <-state.ticker.C:
				return

			default:
				// Fall through.
			}

			btcBlock.Header.Nonce = state.nonce
			hash := btcBlock.Header.BlockHash()
			bitHashRepresentation := pow.HashToBig((*chainhash.Hash)(&hash))
			if bitHashRepresentation.Cmp(state.task.BitcoinBlockTarget) <= 0 {
				m.logInfo().
					Int64("height", state.task.BeaconBlockHeight).
					Stringer("hash", btcBlock.BlockHash()).
					Stringer("pow_hash", hash).
					Msg("btc block mined")

				// Move block to the result struct as finalized.
				state.result.SetBitcoinSolution(btcBlock)
				atLeastOneBlockMined = true
			}

			beaconBlock := state.task.BeaconBlock.Copy()
			beaconBlock.Header.BeaconHeader().SetBTCAux(utils.BtcBlockToBlockAux(btcBlock))

			if bitHashRepresentation.Cmp(state.task.BeaconTarget) <= 0 {
				m.logInfo().
					Int64("height", state.task.BeaconBlockHeight).
					Stringer("hash", beaconBlock.BlockHash()).
					Stringer("pow_hash", hash).
					Msg("beacon block mined")

				// Move block to the result struct as finalized.
				state.result.SetBeaconSolution(beaconBlock)
				atLeastOneBlockMined = true
			}

			for _, t := range state.task.ShardsTargets {
				if bitHashRepresentation.Cmp(t.Target) <= 0 {
					err = state.result.AppendShardSolution(t.ID, t.BlockCandidate, beaconBlock.Header.BeaconHeader())
					if err != nil {
						return
					}

					m.logInfo().
						Int64("height", t.BlockHeight).
						Uint32("shard-id", uint32(t.ID)).
						Stringer("hash", state.task.BeaconBlock.BlockHash()).
						Stringer("pow_hash", hash).
						Msg("shard block mined")
					atLeastOneBlockMined = true

				} else {
					// Other targets are higher than current one.
					// In case if this one could not be achieved - no reason to check another ones.
					break
				}
			}

			if atLeastOneBlockMined {
				return
			}
		}
	}

	return
}

func (m *Miner) solveUsingBeacon(state *context) {
	atLeastOneBlockMined := false
	block := state.task.BeaconBlock

	for ; state.extraNonce < math.MaxUint64; state.extraNonce++ {
		// Update the extra nonce in the block template with the
		// new value by regenerating the coinbase script and
		// setting the merkle root to the new value.
		err := m.UpdateBeaconExtraNonce(state)
		if err != nil {
			m.logErr(err)
			break
		}

		// Search through the entire nonce range for a solution while
		// periodically checking for early quit and stale block
		// conditions along with updates to the speed monitor.
		for ; state.nonce < math.MaxUint32; state.nonce++ {
			select {
			case <-state.ticker.C:
				return

			default:
				// Fall through.
			}

			block.Header.SetNonce(state.nonce)
			hash := block.Header.PoWHash()
			bitHashRepresentation := pow.HashToBig(&hash)

			if bitHashRepresentation.Cmp(state.task.BeaconTarget) <= 0 {
				m.logInfo().
					Int64("height", state.task.BeaconBlockHeight).
					Stringer("hash", block.BlockHash()).
					Stringer("pow_hash", hash).
					Msg("beacon block mined")

				// Move block to the result struct as finalized.
				state.result.SetBeaconSolution(block)
				atLeastOneBlockMined = true
			}

			for _, t := range state.task.ShardsTargets {
				if bitHashRepresentation.Cmp(t.Target) <= 0 {
					err = state.result.AppendShardSolution(t.ID, t.BlockCandidate, block.Header.BeaconHeader())
					if err != nil {
						return
					}

					m.logInfo().
						Int64("height", t.BlockHeight).
						Uint32("shard-id", uint32(t.ID)).
						Stringer("hash", state.task.BeaconBlock.BlockHash()).
						Stringer("pow_hash", hash).
						Msg("shard block mined")
					atLeastOneBlockMined = true

				} else {
					// Other targets are higher than current one.
					// In case if this one could not be achieved - no reason to check another ones.
					break
				}
			}

			if atLeastOneBlockMined {
				return
			}
		}
	}

	return
}

// UpdateBeaconExtraNonce updates the extra nonce in the coinbase script of the passed
// block by regenerating the coinbase script with the passed value and block
// height.  It also recalculates and updates the new merkle root that results
// from changing the coinbase script.
func (m *Miner) UpdateBeaconExtraNonce(state *context) error {
	coinbaseScript, err := standardCoinbaseScript(int32(state.task.BeaconBlockHeight), state.extraNonce)
	if err != nil {
		return err
	}
	if len(coinbaseScript) > chaindata.MaxCoinbaseScriptLen {
		return fmt.Errorf("coinbase transaction script length "+
			"of %d is out of range (min: %d, max: %d)",
			len(coinbaseScript), chaindata.MinCoinbaseScriptLen,
			chaindata.MaxCoinbaseScriptLen)
	}
	state.task.BeaconBlock.Header.UpdateCoinbaseScript(coinbaseScript)

	root := state.task.BeaconBlock.Header.MerkleRoot()
	if root.IsEqual(&chainhash.ZeroHash) {
		// Recalculate the merkle root with the updated extra nonce.
		block := jaxutil.NewBlock(state.task.BeaconBlock)
		merkles := chaindata.BuildMerkleTreeStore(block.Transactions(), false)
		state.task.BeaconBlock.Header.SetMerkleRoot(*merkles[len(merkles)-1])
	}

	return nil
}

// UpdateBitcoinExtraNonce updates the extra nonce in the coinbase script of the passed
// block by regenerating the coinbase script with the passed value and block
// height.  It also recalculates and updates the new merkle root that results
// from changing the coinbase script.
func (m *Miner) UpdateBitcoinExtraNonce(state *context, block *btcdwire.MsgBlock) error {
	coinbaseScript, err := standardCoinbaseScript(int32(state.task.BitcoinBlockHeight), state.extraNonce)
	if err != nil {
		return err
	}
	if len(coinbaseScript) > chaindata.MaxCoinbaseScriptLen {
		return fmt.Errorf("coinbase transaction script length "+
			"of %d is out of range (min: %d, max: %d)",
			len(coinbaseScript), chaindata.MinCoinbaseScriptLen,
			chaindata.MaxCoinbaseScriptLen)
	}
	block.Transactions[0].TxIn[0].SignatureScript = coinbaseScript

	// Recalculate the merkle root with the updated extra nonce.
	block2 := btcdutil.NewBlock(block)
	merkles := blockchain.BuildMerkleTreeStore(block2.Transactions(), false)
	block.Header.MerkleRoot = *merkles[len(merkles)-1]

	return nil
}

// todo: comment
func (m *Miner) UpdateShardsMerkleRoot(state *context) (err error) {
	for _, shard := range state.task.ShardsTargets {

		coinbaseScript, err := standardCoinbaseScript(int32(shard.BlockHeight), state.extraNonce)
		if err != nil {
			return err
		}
		if len(coinbaseScript) > chaindata.MaxCoinbaseScriptLen {
			return fmt.Errorf("coinbase transaction script length "+
				"of %d is out of range (min: %d, max: %d)",
				len(coinbaseScript), chaindata.MinCoinbaseScriptLen,
				chaindata.MaxCoinbaseScriptLen)
		}

		// Recalculate the merkle root with the updated extra nonce.
		shard.BlockCandidate.Header.UpdateCoinbaseScript(coinbaseScript)

		root := shard.BlockCandidate.Header.MerkleRoot()
		if root.IsEqual(&chainhash.ZeroHash) {
			// Recalculate the merkle root with the updated extra nonce.
			block := jaxutil.NewBlock(shard.BlockCandidate)
			merkles := chaindata.BuildMerkleTreeStore(block.Transactions(), false)
			shard.BlockCandidate.Header.SetMerkleRoot(*merkles[len(merkles)-1])
		}
	}

	return
}

// applyHashToCandidate tries to apply received hash to the specified target.
// In case if hash corresponds target - returns true.
// Otherwise - returns false.
func (m *Miner) applyHashToCandidate(target *big.Int, hash *chainhash.Hash, prefixSize int, prefix []byte) bool {
	if bytes.Equal(hash[prefixSize:], prefix) {
		bigFromHash := pow.HashToBig(hash)
		return bigFromHash.Cmp(target) <= 0
	}

	return false
}

func (m *Miner) logTrace() *zerolog.Event {
	return logger.Log.Trace().Str("component", "miner")
}

func (m *Miner) logInfo() *zerolog.Event {
	return logger.Log.Info().Str("component", "miner")
}

func (m *Miner) logErr(err error) *zerolog.Event {
	return logger.Log.Err(err).Str("component", "miner")
}
