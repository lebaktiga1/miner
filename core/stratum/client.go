package stratum

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"math/big"
	"net"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/google/uuid"

	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/stratum/bans"
	"gitlab.com/jaxnet/core/miner/core/stratum/types"
	"gitlab.com/jaxnet/core/miner/core/stratum/vardiff"
)

type Client struct {
	SubscriptionId []byte
	Options        *settings.Configuration
	RemoteAddress  net.Addr

	Socket      net.Conn
	SocketBufIO *bufio.ReadWriter

	LastActivity time.Time
	Shares       *Shares

	IsAuthorized           bool
	SubscriptionBeforeAuth bool

	ExtraNonce1 []byte

	VarDiff *vardiff.VarDiff

	WorkerName string
	WorkerPass string

	PendingDifficulty  *big.Float
	CurrentDifficulty  *big.Float
	PreviousDifficulty *big.Float

	JobManager        *JobManager
	BanningManager    *bans.BanningManager
	SocketClosedEvent chan struct{}
}

func NewStratumClient(subscriptionId []byte, socket net.Conn,
	options *settings.Configuration, bm *bans.BanningManager, jm *JobManager) *Client {

	var varDiff *vardiff.VarDiff

	varDiff = vardiff.NewVarDiff(options)

	return &Client{
		SubscriptionId:         subscriptionId,
		PendingDifficulty:      big.NewFloat(0),
		Options:                options,
		RemoteAddress:          socket.RemoteAddr(),
		Socket:                 socket,
		SocketBufIO:            bufio.NewReadWriter(bufio.NewReader(socket), bufio.NewWriter(socket)),
		LastActivity:           time.Now(),
		IsAuthorized:           false,
		SubscriptionBeforeAuth: false,
		VarDiff:                varDiff,
		BanningManager:         bm,
		JobManager:             jm,
		Shares:                 new(Shares),
		ExtraNonce1:            jm.ExtraNonce1Generator.GetExtraNonce1(),
	}
}

func (sc *Client) ShouldBan(shareValid bool) bool {
	if shareValid {
		sc.Shares.Valid++
	} else {
		sc.Shares.Invalid++
		if sc.Shares.TotalShares() >= sc.Options.Stratum.Banning.CheckThreshold {
			if sc.Shares.BadPercent() < sc.Options.Stratum.Banning.InvalidPercent {
				sc.Shares.Reset()
			} else {
				logger.Log.Info().Str("invalid shares", strconv.FormatUint(sc.Shares.Invalid, 10)).Str("total shares", strconv.FormatUint(sc.Shares.TotalShares(), 10)).Send()
				sc.BanningManager.AddBannedIP(sc.RemoteAddress.String())
				logger.Log.Warn().Str("", sc.WorkerName).Msg("closed socket due to shares bad percent reached the banning invalid percent threshold")
				sc.SocketClosedEvent <- struct{}{}
				_ = sc.Socket.Close()
				return true
			}
		}
	}

	return false
}

func (sc *Client) Init() {
	sc.SetupSocket()
}

func (sc *Client) HandleMessage(message *JsonRpcRequest) {
	switch message.Method {
	case "mining.subscribe":
		sc.HandleSubscribe(message)
	case "mining.authorize":
		sc.HandleAuthorize(message, true)
	case "mining.submit":
		sc.LastActivity = time.Now()
		sc.HandleSubmit(message)
	default:
		logger.Log.Warn().Str("unknown stratum method", string(Jsonify(message))).Send()
	}
}

func (sc *Client) HandleSubscribe(message *JsonRpcRequest) {
	logger.Log.Info().Msg("handling subscribe")
	if !sc.IsAuthorized {
		sc.SubscriptionBeforeAuth = true
	}

	extraNonce2Size := sc.JobManager.ExtraNonce2Size

	sc.SendJsonRPC(&JsonRpcResponse{
		Id: message.Id,
		Result: Jsonify([]interface{}{
			[][]string{
				{"mining.set_difficulty", strconv.FormatUint(binary.LittleEndian.Uint64(sc.SubscriptionId), 10)},
				{"mining.notify", strconv.FormatUint(binary.LittleEndian.Uint64(sc.SubscriptionId), 10)},
			},
			hex.EncodeToString(sc.ExtraNonce1),
			extraNonce2Size,
		}),
		Error: nil,
	})
}

func (sc *Client) HandleAuthorize(message *JsonRpcRequest, replyToSocket bool) {
	logger.Log.Info().Msg("handling authorize")

	sc.WorkerName = string(message.Params[0])
	sc.WorkerPass = string(message.Params[1])

	authorized, disconnect, err := sc.AuthorizeFn(sc.RemoteAddress, sc.Socket.LocalAddr().(*net.TCPAddr).Port, sc.WorkerName, sc.WorkerPass)
	sc.IsAuthorized = err == nil && authorized
	logger.Log.Info().Str("sc.WorkerName", sc.WorkerName).Bool("sc.IsAuthorized", sc.IsAuthorized).Msg("worker authorized")

	if replyToSocket {
		if sc.IsAuthorized {
			sc.SendJsonRPC(&JsonRpcResponse{
				Id:     message.Id,
				Result: Jsonify(sc.IsAuthorized),
				Error:  nil,
			})
		} else {
			sc.SendJsonRPC(&JsonRpcResponse{
				Id:     message.Id,
				Result: Jsonify(sc.IsAuthorized),
				Error: &JsonRpcError{
					Code:    20,
					Message: string(Jsonify(err)),
				},
			})
		}
	}

	if disconnect {
		logger.Log.Warn().Err(err).Str("worker", sc.WorkerName).Msg("closed socket due to failed to authorize the miner")
		_ = sc.Socket.Close()
		sc.SocketClosedEvent <- struct{}{}
	}

	// the init Diff for miners
	logger.Log.Info().Float64("difficulty", sc.Options.Stratum.Ports.Diff).Msg("sending init difficulty")
	sc.SendDifficulty(big.NewFloat(sc.Options.Stratum.Ports.Diff))
	// TODO: fix
	// sc.SendMiningJob(sc.JobManager.CurrentJob.GetJobParams(true))
}

// AuthorizeFn validates worker name and rig name.
func (sc *Client) AuthorizeFn(ip net.Addr, port int, workerName string, password string) (authorized bool, disconnect bool, err error) {
	logger.Log.Info().Str("workerName", workerName).Str("password", password).Str("ipAddr", ip.String()).
		Int("port", port).Msg("Authorize worker")

	var miner, rig string
	workerName = strings.TrimPrefix(workerName, `"`)
	workerName = strings.TrimSuffix(workerName, `"`)
	names := strings.Split(workerName, ".")
	if len(names) < 2 {
		miner = names[0]
		rig = "unknown"
	} else {
		miner = names[0]
		rig = names[1]
	}

	// JMP-64. Miner/rig validation.
	// Miner should be in UUID v4 format.
	_, err = uuid.Parse(miner)
	if err != nil {
		return false, true, fmt.Errorf("%s", types.ErrMinerInvalidFormat)
	}

	// Rig is a string with [a-zA-Z0-9]+ regexp format.
	r, _ := regexp.Compile("[a-zA-Z0-9]+")
	if !r.MatchString(rig) {
		return false, true, fmt.Errorf("%s", types.ErrRigInvalidFormat)
	}

	return true, false, nil
}

func (sc *Client) HandleSubmit(message *JsonRpcRequest) {
	/* Avoid hash flood */
	if !sc.IsAuthorized {
		sc.SendJsonRPC(&JsonRpcResponse{
			Id:     message.Id,
			Result: nil,
			Error: &JsonRpcError{
				Code:    24,
				Message: "unauthorized worker",
			},
		})
		sc.ShouldBan(false)
		return
	}

	if sc.ExtraNonce1 == nil {
		sc.SendJsonRPC(&JsonRpcResponse{
			Id:     message.Id,
			Result: nil,
			Error: &JsonRpcError{
				Code:    25,
				Message: "not subscribed",
			},
		})
		sc.ShouldBan(false)
		return
	}

	share, minerResult := sc.JobManager.ProcessSubmit(
		RawJsonToString(message.Params[1]),
		sc.PreviousDifficulty,
		sc.CurrentDifficulty,
		sc.ExtraNonce1,
		RawJsonToString(message.Params[2]),
		RawJsonToString(message.Params[3]),
		RawJsonToString(message.Params[4]),
		sc.RemoteAddress,
		RawJsonToString(message.Params[0]),
	)

	// sc.JobManager.ProcessShare(share)

	if share.ErrorCode == types.ErrLowDiffShare {
		// warn the miner with current diff
		logger.Log.Error().Str("diff", string(Jsonify([]json.RawMessage{Jsonify(sc.CurrentDifficulty)}))).Msg("Error on handling submit: sending new diff to miner")
		f, _ := sc.CurrentDifficulty.Float64()
		sc.SendJsonRPC(&JsonRpcRequest{
			Id:     nil,
			Method: "mining.set_difficulty",
			Params: []json.RawMessage{Jsonify(f)},
		})

		// return
	}

	if share.ErrorCode == types.ErrNTimeOutOfRange {
		// TODO: not sure what to do here
		// sc.SendMiningJob(sc.JobManager.CurrentJob.GetJobParams(true))
	}

	// vardiff
	if sc.VarDiff != nil {
		diff, _ := sc.CurrentDifficulty.Float64()
		if nextDiff := sc.VarDiff.CalcNextDiff(diff); nextDiff != diff && nextDiff != 0 {
			sc.EnqueueNextDifficulty(nextDiff)
		}
	}

	if sc.PendingDifficulty != nil && sc.PendingDifficulty.Cmp(big.NewFloat(0)) != 0 {
		diff := sc.PendingDifficulty
		logger.Log.Info().Interface("difficulty", diff).Msg("sending new difficulty")
		ok := sc.SendDifficulty(diff)
		sc.PendingDifficulty = nil
		if ok {
			// difficultyChanged
			// -> difficultyUpdate client.workerName, diff
			displayDiff, _ := diff.Float64()
			logger.Log.Info().Float64("diff", displayDiff).Str("workerName", sc.WorkerName).Msg("Difficulty update to diff")
		}
	}

	if sc.ShouldBan(share.ErrorCode == 0) {
		return
	}

	var errParams *JsonRpcError
	if share.ErrorCode != 0 {
		errParams = &JsonRpcError{
			Code:    int(share.ErrorCode),
			Message: share.ErrorCode.String(),
		}

		logger.Log.Error().Str("error message", errParams.Message).Str("worker name", sc.WorkerName).
			Msg("share is invalid")
		sc.SendJsonRPC(&JsonRpcResponse{
			Id:     message.Id,
			Result: Jsonify(false),
			Error:  errParams,
		})

		return
	}

	logger.Log.Info().Str("worker name", sc.WorkerName).Msg("submitted a valid share")
	sc.SendJsonRPC(&JsonRpcResponse{
		Id:     message.Id,
		Result: Jsonify(true),
	})

	// TODO: send result to channel
	sc.JobManager.MinerResultChan <- *minerResult
}

func (sc *Client) SendJsonRPC(jsonRPCs JsonRpc) {
	raw := jsonRPCs.Json()

	message := make([]byte, 0, len(raw)+1)
	message = append(raw, '\n')
	_, err := sc.SocketBufIO.Write(message)
	if err != nil {
		logger.Log.Error().Err(err).Str("raw data", string(raw)).Msg("failed inputting")
	}

	err = sc.SocketBufIO.Flush()
	if err != nil {
		logger.Log.Error().Err(err).Str("worker name", sc.WorkerName).Msg("failed sending data for worker")
	}

	logger.Log.Debug().Str("raw data", string(raw)).Msg("sent raw bytes")
}

func (sc *Client) SendSubscriptionFirstResponse() {
}

func (sc *Client) SetupSocket() {
	sc.BanningManager.CheckBan(sc.RemoteAddress.String())
	once := true

	go func() {
		for {
			select {
			case <-sc.SocketClosedEvent:
				return
			default:
				raw, err := sc.SocketBufIO.ReadBytes('\n')
				if err != nil {
					if err == io.EOF {
						sc.SocketClosedEvent <- struct{}{}
						return
					}
					e, ok := err.(net.Error)

					if !ok {
						logger.Log.Error().Err(err).Msg("failed to ready bytes from socket due to non-network error:")
						return
					}

					if ok && e.Timeout() {
						logger.Log.Error().Err(err).Msg("socket is timeout")
						return
					}

					if ok && e.Temporary() {
						logger.Log.Error().Err(err).Msg("failed to ready bytes from socket due to temporary error")
						continue
					}

					logger.Log.Error().Err(err).Msg("failed to ready bytes from socket:")
					return
				}

				// TODO: remove hardcode
				if len(raw) > 10240 {
					// socketFlooded
					logger.Log.Warn().Str("from", sc.GetLabel()).Str("raw data", string(raw)).
						Msg("Flooding message")
					_ = sc.Socket.Close()
					sc.SocketClosedEvent <- struct{}{}
					return
				}

				if len(raw) == 0 {
					continue
				}

				var message JsonRpcRequest
				err = json.Unmarshal(raw, &message)
				if err != nil {
					if !sc.Options.Stratum.TcpProxyProtocol {
						logger.Log.Error().Str("sc.GetLabel()", sc.GetLabel()).Str("raw data", string(raw)).
							Msg("Malformed message from")
						_ = sc.Socket.Close()
						sc.SocketClosedEvent <- struct{}{}
					}

					return
				}

				if once && sc.Options.Stratum.TcpProxyProtocol {
					once = false
					if bytes.HasPrefix(raw, []byte("PROXY")) {
						sc.RemoteAddress, err = net.ResolveTCPAddr("tcp", string(bytes.Split(raw, []byte(" "))[2]))
						if err != nil {
							logger.Log.Error().Err(err).Msg("failed to resolve tcp addr behind proxy")
						}
					} else {
						logger.Log.Error().Str("raw data", string(raw)).
							Msg("Client IP detection failed, tcpProxyProtocol is enabled yet did not receive proxy protocol message, instead got data")
					}
				}

				sc.BanningManager.CheckBan(sc.RemoteAddress.String())

				if &message != nil {
					logger.Log.Debug().Str("client message", string(message.Json())).Msg("handling message")
					sc.HandleMessage(&message)
				}
			}
		}
	}()
}

func (sc *Client) GetLabel() string {
	if sc.WorkerName != "" {
		return sc.WorkerName + " [" + sc.RemoteAddress.String() + "]"
	} else {
		return "(unauthorized)" + " [" + sc.RemoteAddress.String() + "]"
	}
}

func (sc *Client) EnqueueNextDifficulty(nextDiff float64) bool {
	logger.Log.Info().Float64("nextDiff", nextDiff).Msg("Enqueue next difficulty")
	sc.PendingDifficulty = big.NewFloat(nextDiff)
	return true
}

func (sc *Client) SendDifficulty(diff *big.Float) bool {
	if diff == nil {
		logger.Log.Fatal().Msg("trying to send empty diff!")
	}
	if sc.CurrentDifficulty != nil && diff.Cmp(sc.CurrentDifficulty) == 0 {
		return false
	}

	sc.PreviousDifficulty = sc.CurrentDifficulty
	sc.CurrentDifficulty = diff

	f, _ := diff.Float64()
	sc.SendJsonRPC(&JsonRpcRequest{
		Id:     0,
		Method: "mining.set_difficulty",
		Params: []json.RawMessage{Jsonify(f)},
	})

	return true
}

func (sc *Client) SendMiningJob(jobParams []interface{}) {
	// logger.Log.Info().Str("job", string(Jsonify(jobParams))).Msg("sending job")

	lastActivityAgo := time.Since(sc.LastActivity)
	if lastActivityAgo > time.Duration(sc.Options.Stratum.ConnectionTimeout)*time.Second {
		logger.Log.Info().Str("sc.WorkerName", sc.WorkerName).Msg("closed socket due to activity timeout")
		_ = sc.Socket.Close()
		sc.SocketClosedEvent <- struct{}{}
		return
	}

	if sc.PendingDifficulty != nil && sc.PendingDifficulty.Cmp(big.NewFloat(0)) != 0 {
		diff := sc.PendingDifficulty
		ok := sc.SendDifficulty(diff)
		sc.PendingDifficulty = nil
		if ok {
			// difficultyChanged
			// -> difficultyUpdate client.workerName, diff
			displayDiff, _ := diff.Float64()
			logger.Log.Info().Str("worker", sc.WorkerName).Float64("diff", displayDiff).Msg("Difficulty update to diff")
		}
	}

	params := make([]json.RawMessage, len(jobParams))
	for i := range jobParams {
		params[i] = Jsonify(jobParams[i])
	}

	sc.SendJsonRPC(&JsonRpcRequest{
		Id:     nil,
		Method: "mining.notify",
		Params: params,
	})
}

func (sc *Client) ManuallyAuthClient(username, password string) {
	sc.HandleAuthorize(&JsonRpcRequest{
		Id:     1,
		Method: "",
		Params: []json.RawMessage{Jsonify(username), Jsonify(password)},
	}, false)
}

func (sc *Client) ManuallySetValues(otherClient *Client) {
	sc.ExtraNonce1 = otherClient.ExtraNonce1
	sc.PreviousDifficulty = otherClient.PreviousDifficulty
	sc.CurrentDifficulty = otherClient.CurrentDifficulty
}
