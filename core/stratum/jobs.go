package stratum

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"math/big"
	"net"
	"strconv"
	"strings"
	"time"

	"github.com/btcsuite/btcd/blockchain"
	btcdwire "github.com/btcsuite/btcd/wire"
	btcdutil "github.com/btcsuite/btcutil"

	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/stratum/types"
	"gitlab.com/jaxnet/core/miner/core/stratum/utils"
	utils2 "gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/pow"
)

type JobCounter struct {
	Counter *big.Int
}

type JobManager struct {
	Config                *settings.Configuration
	JobCounter            *JobCounter
	ExtraNonce1Generator  *ExtraNonce1Generator
	ExtraNoncePlaceholder []byte
	ExtraNonce2Size       int

	CurrentJob *Job
	ValidJobs  map[string]*Job

	// Not sure if this needed
	CoinbaseHasher func([]byte) chainhash.Hash

	MinerResultChan chan tasks.MinerResult
}

type Job struct {
	MinerTask             *tasks.MinerTask
	JobId                 string
	PrevHashReversed      string
	Target                *big.Int
	Difficulty            *big.Float
	Submits               []string
	GenerationTransaction [][]byte
}

type Share struct {
	JobId      string          `json:"jobId"`
	RemoteAddr net.Addr        `json:"remoteAddr"`
	Miner      string          `json:"miner"`
	Rig        string          `json:"rig"`
	ErrorCode  types.ErrorWrap `json:"errorCode"`
	DiffBigInt *big.Int        `json:"diffBigInt"`
	// TODO: not sure if fields below are needed
	BlockHeight int64   `json:"height"`
	BlockReward uint64  `json:"blockReward"`
	Diff        float64 `json:"shareDiff"`
	BlockHash   string  `json:"blockHash"`
	BlockHex    string  `json:"blockHex"`
	TxHash      string  `json:"txHash"`
}

type ExtraNonce1Generator struct {
	Size int
}

func NewExtraNonce1Generator() *ExtraNonce1Generator {
	return &ExtraNonce1Generator{
		Size: 4,
	}
}

func (eng *ExtraNonce1Generator) GetExtraNonce1() []byte {
	extraNonce := make([]byte, eng.Size)
	_, _ = rand.Read(extraNonce)

	return extraNonce
}

func (jm *JobManager) ProcessSubmit(jobId string, prevDiff, diff *big.Float, extraNonce1 []byte,
	hexExtraNonce2, hexNTime, hexNonce string, ipAddr net.Addr, workerName string) (share *Share, minerResult *tasks.MinerResult) {
	minerResult = new(tasks.MinerResult)

	submitTime := time.Now()

	logger.Log.Info().Str("jobId", jobId).Interface("prevDiff", prevDiff).Bytes("extraNonce1", extraNonce1).
		Str("hexExtraNonce2", hexExtraNonce2).Str("hexNTime", hexNTime).Str("hexNonce", hexNonce).
		Interface("ipAddr", ipAddr).Str("workerName", workerName).
		Msg("*** Process submit")

	var miner, rig string
	names := strings.Split(workerName, ".")
	if len(names) < 2 {
		miner = names[0]
		rig = "unknown"
	} else {
		miner = names[0]
		rig = names[1]
	}

	job, exists := jm.ValidJobs[jobId]
	if !exists || job == nil || job.JobId != jobId {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrJobNotFound,
		}, nil
	}

	extraNonce2, err := hex.DecodeString(hexExtraNonce2)
	if err != nil {
		logger.Log.Error().Err(err).Send()
	}

	if len(extraNonce2) != jm.ExtraNonce2Size {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrIncorrectExtraNonce2Size,
		}, nil
	}

	if len(hexNTime) != 8 {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrIncorrectNTimeSize,
		}, nil
	}

	// allowed nTime range [GBT's CurTime, submitTime+7s]
	// GBT check is dropped.
	nTimeInt, err := strconv.ParseInt(hexNTime, 16, 64)
	if err != nil {
		logger.Log.Error().Err(err).Send()
	}
	if nTimeInt > submitTime.Unix()+7 {
		logger.Log.Error().Msgf("nTime incorrect: expect less %d got %d", submitTime.Unix()+7, uint32(nTimeInt))
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrNTimeOutOfRange,
		}, nil
	}

	if len(hexNonce) != 8 {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrIncorrectNonceSize,
		}, nil
	}

	if !job.RegisterSubmit(hex.EncodeToString(extraNonce1), hexExtraNonce2, hexNTime, hexNonce) {
		return &Share{
			JobId:      jobId,
			RemoteAddr: ipAddr,
			Miner:      miner,
			Rig:        rig,

			ErrorCode: types.ErrDuplicateShare,
		}, nil
	}

	nonce, err := strconv.ParseInt(hexNonce, 16, 64)
	if err != nil {
		logger.Log.Error().Err(err).Send()
	}

	// hexExtraNonce2, hexNonce string
	extraNonce, err := strconv.ParseInt(hexExtraNonce2, 16, 64)
	if err != nil {
		logger.Log.Error().Err(err).Send()
	}

	// Copy current block and check for required difficulty for bitcoin block
	block := utils2.BitcoinBlockCopy(jm.CurrentJob.MinerTask.BitcoinBlock)
	block.Header.Nonce = uint32(nonce)

	// TODO: not sure if it is needed
	jm.UpdateBitcoinExtraNonce(block, uint64(extraNonce))

	hash := block.Header.BlockHash()
	bitHashRepresentation := pow.HashToBig((*chainhash.Hash)(&hash))

	share = &Share{
		JobId:      jobId,
		RemoteAddr: ipAddr,
		Miner:      miner,
		Rig:        rig,
		DiffBigInt: bitHashRepresentation,
	}

	atLeastOneBlockMined := false
	logger.Log.Info().Msg("Checking submitted block difficulty")

	logger.Log.Info().Str("Miner target", bitHashRepresentation.Text(16)).
		Str("beacon target", jm.CurrentJob.MinerTask.BeaconTarget.Text(16)).
		Str("bitcoin target", jm.CurrentJob.MinerTask.BitcoinBlockTarget.Text(16)).
		Send()

	if bitHashRepresentation.Cmp(jm.CurrentJob.MinerTask.BitcoinBlockTarget) <= 0 {
		logger.Log.Warn().Str("blockHash", hash.String()).Msg("Found bitcoin block")
		atLeastOneBlockMined = true
		minerResult.SetBitcoinSolution(block)
	}

	if bitHashRepresentation.Cmp(jm.CurrentJob.MinerTask.BeaconTarget) <= 0 {
		logger.Log.Warn().Str("blockHash", hash.String()).Msg("Found beacon block")
		atLeastOneBlockMined = true
		beaconBlock := jm.CurrentJob.MinerTask.BeaconBlock.Copy()
		beaconBlock.Header.BeaconHeader().SetBTCAux(utils2.BtcBlockToBlockAux(block))
		minerResult.SetBeaconSolution(jm.CurrentJob.MinerTask.BeaconBlock)
	}

	for _, t := range jm.CurrentJob.MinerTask.ShardsTargets {
		logger.Log.Info().Str("Miner target", bitHashRepresentation.String()).Str("shard target", t.Target.String()).Send()

		if bitHashRepresentation.Cmp(t.Target) <= 0 {
			beaconBlock := jm.CurrentJob.MinerTask.BeaconBlock.Copy()
			beaconBlock.Header.BeaconHeader().SetBTCAux(utils2.BtcBlockToBlockAux(block))
			err = minerResult.AppendShardSolution(t.ID, t.BlockCandidate, beaconBlock.Header.BeaconHeader())
			if err != nil {
				return
			}

			logger.Log.Info().Int64(
				"height", t.BlockHeight).Uint32(
				"shard-id", uint32(t.ID)).Msg("shard block mined")
			atLeastOneBlockMined = true

		} else {
			// Other targets are higher than current one.
			// In case if this one could not be achieved - no reason to check another ones.
			break
		}
	}

	if atLeastOneBlockMined {
		logger.Log.Info().Msg("At least one block mined")
		return share, minerResult
	} else {
		return &Share{
			ErrorCode: types.ErrLowDiffShare,
		}, nil
	}
}

func (j *Job) RegisterSubmit(extraNonce1, extraNonce2, nTime, nonce string) bool {
	submission := extraNonce1 + extraNonce2 + nTime + nonce

	if utils.StringsIndexOf(j.Submits, submission) == -1 {
		j.Submits = append(j.Submits, submission)
		return true
	}

	return false
}

func (jm *JobManager) UpdateBitcoinExtraNonce(block *btcdwire.MsgBlock, extraNonce uint64) error {
	coinbaseScript, err := standardCoinbaseScript(int32(jm.CurrentJob.MinerTask.BitcoinBlockHeight), extraNonce)
	if err != nil {
		return err
	}
	if len(coinbaseScript) > chaindata.MaxCoinbaseScriptLen {
		return fmt.Errorf("coinbase transaction script length "+
			"of %d is out of range (min: %d, max: %d)",
			len(coinbaseScript), chaindata.MinCoinbaseScriptLen,
			chaindata.MaxCoinbaseScriptLen)
	}
	block.Transactions[0].TxIn[0].SignatureScript = coinbaseScript

	// Recalculate the merkle root with the updated extra nonce.
	block2 := btcdutil.NewBlock(block)

	merkles := blockchain.BuildMerkleTreeStore(block2.Transactions(), false)
	block.Header.MerkleRoot = *merkles[len(merkles)-1]
	return nil
}

func NewJobManager(config *settings.Configuration, minerResultChan chan tasks.MinerResult) *JobManager {
	placeholder, _ := hex.DecodeString("f000000ff111111f")
	extraNonce1Generator := NewExtraNonce1Generator()

	// job := new(Job)
	// job.MinerTask = new(tasks.MinerTask)

	jm := JobManager{
		Config:                config,
		ExtraNonce1Generator:  extraNonce1Generator,
		ExtraNoncePlaceholder: placeholder,
		ExtraNonce2Size:       len(placeholder) - extraNonce1Generator.Size,
		CurrentJob:            nil,
		ValidJobs:             make(map[string]*Job),
		CoinbaseHasher:        chainhash.DoubleHashH,
		MinerResultChan:       minerResultChan,
	}

	return &jm
}

func (jm *JobManager) GetJobParamsEx(job *Job, forceUpdate bool) []interface{} {
	beaconChainBlockHash := job.MinerTask.BeaconBlock.BlockHash().String()
	logger.Log.Info().Str("beaconChainBlockHash", beaconChainBlockHash).Send()

	prevHashBytes := job.MinerTask.BitcoinBlock.Header.PrevBlock.CloneBytes()
	prevHashReversed := hex.EncodeToString(utils.ReverseByteOrder(prevHashBytes))
	parts := CreateCoinbaseTransactionParts(beaconChainBlockHash, job.MinerTask.BitcoinBlockHeight, jm.ExtraNoncePlaceholder)
	block := btcdutil.NewBlock(job.MinerTask.BitcoinBlock)
	merkles := blockchain.BuildMerkleTreeStore(block.Transactions(), false)
	// logger.Log.Info().Interface("*** len(merkles)", len(merkles)).Send()
	merklePath := utils2.MerkleTreeBranch(merkles)
	// logger.Log.Info().Interface("*** len(merklePath)", len(merklePath)).Send()

	merkleBranch := make([]string, 0)
	for _, merkle := range merklePath {
		if merkle != nil {
			merkleBranch = append(merkleBranch, merkle.String())
		}
	}

	// logger.Log.Info().Interface("*** merkleBranch", merkleBranch).Send()

	jm.ValidJobs[job.JobId] = job

	var validJobIDs []string
	for k := range jm.ValidJobs {
		validJobIDs = append(validJobIDs, k)
	}
	// logger.Log.Info().Interface("valid job IDs", validJobIDs).Send()

	return []interface{}{
		job.JobId,
		prevHashReversed,
		hex.EncodeToString(parts[0]),
		hex.EncodeToString(parts[1]),
		merkleBranch,
		hex.EncodeToString(utils.PackInt32BE(job.MinerTask.BitcoinBlockVersion)),
		hex.EncodeToString(utils.PackUint32BE(job.MinerTask.BitcoinBlockBits)),
		hex.EncodeToString(utils.PackUint32BE(uint32(time.Now().Unix()))), // Updated: implement time rolling
		forceUpdate,
	}
}

func CreateCoinbaseTransactionParts(beaconChainBlockHash string, bitcoinBlockHeight int64, extraNoncePlaceholder []byte) [][]byte {
	txVersion := 1
	txComment := make([]byte, 0)
	txType := 0
	txVersion = txVersion + (txType << 16)

	txInPrevOutHash := ""
	txInPrevOutIndex := 1<<32 - 1
	txInSequence := 0

	txTimestamp := make([]byte, 0)
	txLockTime := 0

	// TODO: incorrect?
	bCoinbaseAuxFlags := []byte("")
	// bCoinbaseAuxFlags, err := hex.DecodeString(bitcoinBC.CoinbaseAux.Flags)
	// if err != nil {
	// 	logger.Log.Error().Err(err).Send()
	// }
	scriptSigPart1 := bytes.Join([][]byte{
		utils.SerializeNumber(uint64(bitcoinBlockHeight)),
		bCoinbaseAuxFlags,
		utils.SerializeNumber(uint64(time.Now().Unix())),
		{byte(len(extraNoncePlaceholder))},
	}, nil)

	// JMP-64.
	byCommand := []byte("/by Command/")
	bytesTrash := make([]byte, 0)

	bytesTrash = append(bytesTrash, byCommand...)
	bytesTrash = append(bytesTrash, []byte(beaconChainBlockHash)...)
	// logger.Log.Info().Str("bytesTrash", string(bytesTrash)).Send()
	// pattern := []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	// for i := 0; i < 10; i++ {
	// 	bytesTrash = append(bytesTrash, pattern...)
	// }

	scriptSigPart2 := utils.SerializeString(beaconChainBlockHash)

	p1 := bytes.Join([][]byte{
		utils.PackUint32LE(uint32(txVersion)),
		txTimestamp,

		// transaction input
		utils.VarIntBytes(1), // only one txIn
		utils.Uint256BytesFromHash(txInPrevOutHash),
		utils.PackUint32LE(uint32(txInPrevOutIndex)),
		utils.VarIntBytes(uint64(len(scriptSigPart1) + len(extraNoncePlaceholder) + len(scriptSigPart2))),
		scriptSigPart1,
	}, nil)

	outputTransactions := GenerateOutputTransactions()

	p2 := bytes.Join([][]byte{
		scriptSigPart2,
		utils.PackUint32LE(uint32(txInSequence)),
		// end transaction input

		// transaction output
		outputTransactions,
		// end transaction output

		utils.PackUint32LE(uint32(txLockTime)),
		txComment,
	}, nil)

	return [][]byte{p1, p2}
}

func GenerateOutputTransactions() []byte {
	txOutputBuffers := make([][]byte, 0)

	return bytes.Join([][]byte{
		utils.VarIntBytes(uint64(len(txOutputBuffers))),
		bytes.Join(txOutputBuffers, nil),
	}, nil)
}
