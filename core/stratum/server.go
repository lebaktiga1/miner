package stratum

import (
	"encoding/binary"
	"fmt"
	"net"
	"time"

	"github.com/rs/zerolog"

	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/state"
	"gitlab.com/jaxnet/core/miner/core/stratum/bans"
	sutils "gitlab.com/jaxnet/core/miner/core/stratum/utils"
	"gitlab.com/jaxnet/core/miner/core/stratum/vardiff"
	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/jaxutil"
	"gitlab.com/jaxnet/jaxnetd/node/chaindata"
	"gitlab.com/jaxnet/jaxnetd/txscript"
)

// Server describes structure of the stratum server.
type Server struct {
	utils.StoppableMixin
	stopped             bool
	Config              *settings.Configuration
	Listener            net.Listener
	VarDiff             *vardiff.VarDiff
	StratumClients      map[uint64]*Client
	SubscriptionCounter *SubscriptionCounter
	BanningManager      *bans.BanningManager
	JobManager          *JobManager
	rebroadcastTicker   *time.Ticker
}

func NewStratumServer(options *settings.Configuration) *Server {
	return &Server{
		Config:              options,
		BanningManager:      bans.NewBanningManager(options),
		SubscriptionCounter: NewSubscriptionCounter(),
		StratumClients:      make(map[uint64]*Client),
	}
}

func (ss *Server) Init(minerResultChan chan tasks.MinerResult) {
	if ss.Config.Stratum.Banning.Enabled {
		ss.BanningManager.Init()
	}

	ss.JobManager = NewJobManager(ss.Config, minerResultChan)

	var err error
	if ss.Config.Stratum.Ports.TLS {
		// TODO: fix
		// ss.Listener, err = tls.Listen("tcp", ":"+strconv.Itoa(port), options.TLS.ToTLSConfig())
	} else {
		// ss.Listener, err = net.Listen("tcp", ":"+strconv.Itoa(port))
		ss.Listener, err = net.Listen("tcp4", ":"+ss.Config.Stratum.Ports.Port)
	}

	if err != nil {
		logger.Log.Panic().Err(err).Msg("Failed to start stratum server")
	} else {
		logger.Log.Info().Str("port", ss.Config.Stratum.Ports.Port).Msg("Stratum server started")
	}

	go func() {
		ss.rebroadcastTicker = time.NewTicker(time.Duration(ss.Config.Stratum.JobRebroadcastTimeout) * 10 * time.Millisecond)
		defer logger.Log.Warn().Msg("broadcaster stopped")
		defer ss.rebroadcastTicker.Stop()
		for {
			<-ss.rebroadcastTicker.C
			if ss.JobManager.CurrentJob != nil && ss.JobManager.CurrentJob.MinerTask != nil {
				go ss.BroadcastCurrentMiningJob(ss.JobManager.GetJobParamsEx(ss.JobManager.CurrentJob,
					false,
				))
			}
		}
	}()

	go func() {
		for {
			conn, err := ss.Listener.Accept()
			if err != nil {
				logger.Log.Error().Err(err).Send()
				continue
			}

			if conn != nil {
				logger.Log.Info().Str("from", conn.RemoteAddr().String()).Msg("new miner conn")
				go ss.HandleNewClient(conn)
			}
		}
	}()
}

// HandleNewClient converts the conn to an underlying client instance and finally return its unique subscriptionID
func (ss *Server) HandleNewClient(socket net.Conn) []byte {
	subscriptionID := ss.SubscriptionCounter.Next()
	client := NewStratumClient(subscriptionID, socket, ss.Config, ss.BanningManager, ss.JobManager)
	ss.StratumClients[binary.LittleEndian.Uint64(subscriptionID)] = client
	// client.connected

	go func() {
		for {
			<-client.SocketClosedEvent
			logger.Log.Warn().Msg("a client socket closed")
			ss.RemoveStratumClientBySubscriptionId(subscriptionID)
			// client.disconnected
		}
	}()

	client.Init()

	return subscriptionID
}

func (ss *Server) BroadcastCurrentMiningJob(jobParams []interface{}) {
	logger.Log.Info().Msg("broadcasting job params")
	for clientId := range ss.StratumClients {
		ss.StratumClients[clientId].SendMiningJob(jobParams)
	}
}

func (ss *Server) RemoveStratumClientBySubscriptionId(subscriptionId []byte) {
	delete(ss.StratumClients, binary.LittleEndian.Uint64(subscriptionId))
}

func (ss *Server) ManuallyAddStratumClient(client *Client) {
	subscriptionId := ss.HandleNewClient(client.Socket)
	if subscriptionId != nil {
		ss.StratumClients[binary.LittleEndian.Uint64(subscriptionId)].ManuallyAuthClient(client.WorkerName, client.WorkerPass)
		ss.StratumClients[binary.LittleEndian.Uint64(subscriptionId)].ManuallySetValues(client)
	}
}

// RunUsing sends current mining task to miner.
func (ss *Server) RunUsing(c *state.Coordinator) {
	var (
		ctx      *tasks.Context
		nextTask *tasks.MinerTask
	)

	for {
		if ss.MustBeStopped {
			ss.stopped = true
			return
		}

		nextTask = c.NextTask()
		if nextTask == nil && ctx == nil {
			// There is no work available for miner.
			// Even if current task is not nil - no mining must be done,
			// because the actual BC header has been set to nil on the ctx's side.
			//
			// Wait some time and check again later.
			time.Sleep(tasks.MiningRoundTimeWindow)
			continue
		}

		if ctx == nil || nextTask != ctx.Task {
			// The task has been updated, so the mining context must be reset.
			// In all other cases the mining would be continued with previous context.
			ss.JobManager.CurrentJob = &Job{
				// TODO: use something more intelligent
				JobId:     sutils.RandHexUint64(),
				MinerTask: nextTask,
			}
		}
	}
}

func (ss *Server) UpdateShardsMerkleRoot(state *tasks.Context) (err error) {
	for _, shard := range state.Task.ShardsTargets {
		coinbaseScript, err := standardCoinbaseScript(int32(shard.BlockHeight), state.ExtraNonce)
		if err != nil {
			return err
		}
		if len(coinbaseScript) > chaindata.MaxCoinbaseScriptLen {
			return fmt.Errorf("coinbase transaction script length "+
				"of %d is out of range (min: %d, max: %d)",
				len(coinbaseScript), chaindata.MinCoinbaseScriptLen,
				chaindata.MaxCoinbaseScriptLen)
		}
		shard.BlockCandidate.Transactions[0].TxIn[0].SignatureScript = coinbaseScript

		// Recalculate the merkle root with the updated extra nonce.
		block := jaxutil.NewBlock(shard.BlockCandidate)
		merkles := chaindata.BuildMerkleTreeStore(block.Transactions(), false)
		shard.BlockCandidate.Header.SetMerkleRoot(*merkles[len(merkles)-1])
	}

	return
}

// standardCoinbaseScript returns a standard script suitable for use as the
// signature script of the coinbase transaction of a new block.  In particular,
// it starts with the block height that is required by version 2 blocks and adds
// the extra nonce as well as additional coinbase flags.
func standardCoinbaseScript(nextBlockHeight int32, extraNonce uint64) ([]byte, error) {
	return txscript.NewScriptBuilder().AddInt64(int64(nextBlockHeight)).
		AddInt64(int64(extraNonce)).AddData([]byte(tasks.CoinbaseFlags)).
		Script()
}

const loggerComponent = "stratum server"

func (ss *Server) logTrace() *zerolog.Event {
	return logger.Log.Trace().Str("component", loggerComponent)
}

func (ss *Server) logInfo() *zerolog.Event {
	return logger.Log.Info().Str("component", loggerComponent)
}

func (ss *Server) logErr(err error) *zerolog.Event {
	return logger.Log.Err(err).Str("component", loggerComponent)
}
