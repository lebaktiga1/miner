package vardiff

import (
	"time"

	"gitlab.com/jaxnet/core/miner/core/settings"
)

type VarDiff struct {
	Options       *settings.Configuration
	BufferSize    int64
	MaxTargetTime float64
	MinTargetTime float64

	TimeBuffer    *RingBuffer
	LastRtc       int64
	LastTimestamp int64
}

func NewVarDiff(options *settings.Configuration) *VarDiff {
	timestamp := time.Now().Unix()
	bufferSize := options.Stratum.Ports.VarDiff.RetargetTime / options.Stratum.Ports.VarDiff.TargetTime * 4
	return &VarDiff{
		Options:       options,
		BufferSize:    bufferSize,
		MaxTargetTime: float64(options.Stratum.Ports.VarDiff.TargetTime) * (1 + options.Stratum.Ports.VarDiff.VariancePercent),
		MinTargetTime: float64(options.Stratum.Ports.VarDiff.TargetTime) * (1 - options.Stratum.Ports.VarDiff.VariancePercent),
		TimeBuffer:    NewRingBuffer(bufferSize),
		LastRtc:       timestamp - options.Stratum.Ports.VarDiff.RetargetTime/2,
		LastTimestamp: timestamp,
	}
}

func (vd *VarDiff) CalcNextDiff(currentDiff float64) (newDiff float64) {
	timestamp := time.Now().Unix()

	if vd.LastRtc == 0 {
		vd.LastRtc = timestamp - vd.Options.Stratum.Ports.VarDiff.RetargetTime/2
		vd.LastTimestamp = timestamp
		return
	}

	sinceLast := timestamp - vd.LastTimestamp

	vd.TimeBuffer.Append(sinceLast)
	vd.LastTimestamp = timestamp

	if (timestamp-vd.LastRtc) < vd.Options.Stratum.Ports.VarDiff.RetargetTime && vd.TimeBuffer.Size() > 0 {
		return
	}

	vd.LastRtc = timestamp
	avg := vd.TimeBuffer.Avg()
	ddiff := float64(time.Duration(vd.Options.Stratum.Ports.VarDiff.TargetTime)*time.Second) / avg

	// currentDiff, _ := client.Difficulty.Float64()

	if avg > vd.MaxTargetTime && currentDiff > vd.Options.Stratum.Ports.VarDiff.MinDiff {
		if vd.Options.Stratum.Ports.VarDiff.X2Mode {
			ddiff = 0.5
		}

		if ddiff*currentDiff < vd.Options.Stratum.Ports.VarDiff.MinDiff {
			ddiff = vd.Options.Stratum.Ports.VarDiff.MinDiff / currentDiff
		}
	} else if avg < vd.MinTargetTime {
		if vd.Options.Stratum.Ports.VarDiff.X2Mode {
			ddiff = 2
		}

		diffMax := vd.Options.Stratum.Ports.VarDiff.MaxDiff

		if ddiff*currentDiff > diffMax {
			ddiff = diffMax / currentDiff
		}
	} else {
		return currentDiff
	}

	newDiff = currentDiff * ddiff

	if newDiff <= 0 {
		newDiff = currentDiff
	}

	vd.TimeBuffer.Clear()
	return
}
