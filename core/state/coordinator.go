/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package state

import (
	"math/big"
	"sort"
	"sync"
	"time"

	btcdchainhash "github.com/btcsuite/btcd/chaincfg/chainhash"

	btcdwire "github.com/btcsuite/btcd/wire"
	"gitlab.com/jaxnet/core/miner/core/common"
	"gitlab.com/jaxnet/core/miner/core/e"
	"gitlab.com/jaxnet/core/miner/core/logger"
	"gitlab.com/jaxnet/core/miner/core/settings"
	"gitlab.com/jaxnet/core/miner/core/utils"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"

	mm "gitlab.com/jaxnet/core/merged-mining-tree"
	"gitlab.com/jaxnet/core/miner/core/communicator/events"
	"gitlab.com/jaxnet/core/miner/core/miner/tasks"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

type shard struct {
	block   *wire.MsgBlock
	target  *big.Int
	updated time.Time
	height  int64
}

type beacon struct {
	block  *wire.MsgBlock
	target *big.Int
	height int64
}

type bitcoin struct {
	block  *btcdwire.MsgBlock
	target *big.Int
	height int64
}

type Coordinator struct {
	utils.StoppableMixin

	config  *settings.Configuration
	shards  map[common.ShardID]*shard
	beacon  *beacon
	bitcoin *bitcoin

	// Stores the hash of the block, that was originally received from the node.
	// It helps deduplicate blocks flow from the node and
	// update miner's task only if the original block has been changed also.
	originalBeaconBlockHash  chainhash.Hash
	originalBitcoinBlockHash btcdchainhash.Hash

	task      *tasks.MinerTask
	taskMutex sync.Mutex
}

func New(conf *settings.Configuration) (handler *Coordinator) {
	handler = &Coordinator{
		config: conf,
		shards: make(map[common.ShardID]*shard),
	}

	handler.IsStopped = func() bool {
		// todo: provide implementation
		panic("not implemented")
	}

	return
}

func (h *Coordinator) RunUsing(
	shardsBlockCandidates <-chan events.ShardBlockCandidate,
	beaconBlockCandidates <-chan events.BeaconBlockCandidate,
	bitcoinBlockCandidates <-chan events.BitcoinBlockCandidate) {

	for {
		if h.MustBeStopped {
			return
		}

		select {
		case eventBeaconCandidate := <-beaconBlockCandidates:
			h.processBeaconCandidate(eventBeaconCandidate)

		case eventShardCandidate := <-shardsBlockCandidates:
			h.processShardCandidate(eventShardCandidate)

		case eventBitcoinCandidate := <-bitcoinBlockCandidates:
			h.processBitcoinCandidate(eventBitcoinCandidate)
		}

	}
}

func (h *Coordinator) NextTask() *tasks.MinerTask {
	h.taskMutex.Lock()
	defer h.taskMutex.Unlock()

	return h.task
}

func (h *Coordinator) processShardCandidate(e events.ShardBlockCandidate) {
	if h.beacon == nil {
		logger.Log.Warn().Uint32(
			"shard-id", uint32(e.ShardID)).Msg(
			"Can't apply shard block candidate. No beacon block set yet.")

		// in case if there is no beacon block set - no shard block candidate could be set too.
		return
	}

	block, target, height, err := h.decodeShardBlockTemplateResponse(e.Candidate)
	if err != nil {
		logger.Log.Err(err).Msg("Can't decode shard block template response")
		return
	}

	// todo: add the sme deduplication mechanics as was added for beacon block.
	//		 (see processBeaconCandidate() method for the details)
	err = h.setNewShardCandidate(e.ShardID, block, target, height)
	if err != nil {
		logger.Log.Err(err).Msg("Can't update shard block candidate")
		return
	}

	h.updateTask()
}

func (h *Coordinator) processBeaconCandidate(event events.BeaconBlockCandidate) {
	block, target, height, err := h.decodeBeaconResponse(event.Candidate)
	if err != nil {
		logger.Log.Err(err).Msg("Can't decode beacon block template response")
		return
	}

	err = h.setNewBeaconBlockCandidate(block, target, height)
	if err != nil {
		if err == e.ErrDuplicatedBlock {
			// Core reported the same block as previous one.
			// No need to update the task.
			// No error to report.
			return

		} else {
			logger.Log.Err(err).Msg("Can't set beacon block candidate")
			return
		}
	}

	h.updateTask()
}

func (h *Coordinator) processBitcoinCandidate(event events.BitcoinBlockCandidate) {
	block, target, height, err := h.decodeBitcoinResponse(event.Candidate)
	if err != nil {
		logger.Log.Err(err).Msg("Can't decode beacon block template response")
		return
	}

	err = h.setNewBitcoinBlockCandidate(block, target, height)
	if err != nil {
		if err == e.ErrDuplicatedBlock {
			// Core reported the same block as previous one.
			// No need to update the task.
			// No error to report.
			return

		} else {
			logger.Log.Err(err).Msg("Can't set beacon block candidate")
			return
		}
	}

	h.updateTask()
}

func (h *Coordinator) setNewShardCandidate(
	shardID common.ShardID, block *wire.MsgBlock, target *big.Int, height int64) (err error) {

	shardRecord, isPresent := h.shards[shardID]
	if !isPresent {
		shardRecord = &shard{}
		h.shards[shardID] = shardRecord
	}

	shardRecord.block = block
	shardRecord.block.Header.(*wire.ShardHeader).SetMergeMiningNumber(uint32(len(h.shards)))
	shardRecord.target = target
	shardRecord.updated = time.Now()
	shardRecord.height = height

	err = h.updateMergedMiningProof()
	return
}

func (h *Coordinator) setNewBeaconBlockCandidate(
	block *wire.MsgBlock, target *big.Int, height int64) (err error) {

	if h.beacon == nil {
		h.beacon = &beacon{}
	}

	// In case if core reports the same block that was received earlier -
	// do not update the task, so the miner's state would not be changed
	// (higher probability that the task would be finished and the block would be mined,
	// because no nonce reset would occur).
	if h.beacon.block != nil {

		// New block hash is compared with cached beacon block hash.
		// The block hash is used because block itself is stored as pointer,
		// and when it reaches the miner - it's header is populated with timestamp and nonce,
		// that is leading ot the hash changes and failed comparison operations
		// even if original blocks are the same.
		newBlockHash := block.Header.BlockHash()
		if newBlockHash.IsEqual(&h.originalBeaconBlockHash) {
			err = e.ErrDuplicatedBlock
			return
		}
	}

	h.beacon.block = block
	beaconHeader := block.Header.BeaconHeader()
	lastBCHeader = beaconHeader.Copy().BeaconHeader()
	lastBCCoinbaseAux = wire.CoinbaseAux{
		Tx:       *block.Transactions[0].Copy(),
		TxMerkle: make([]chainhash.Hash, len(block.Transactions)),
	}
	for i, tx := range block.Transactions {
		lastBCCoinbaseAux.TxMerkle[i] = tx.TxHash()
	}

	h.originalBeaconBlockHash = block.Header.BlockHash()
	h.beacon.target = target
	h.beacon.height = height

	err = h.updateMergedMiningProof()
	return
}

func (h *Coordinator) setNewBitcoinBlockCandidate(
	block *btcdwire.MsgBlock, target *big.Int, height int64) (err error) {

	if h.bitcoin == nil {
		h.bitcoin = &bitcoin{}
	}

	// In case if core reports the same block that was received earlier -
	// do not update the task, so the miner's state would not be changed
	// (higher probability that the task would be finished and the block would be mined,
	// because no nonce reset would occur).
	if h.bitcoin.block != nil {

		// New block hash is compared with cached beacon block hash.
		// The block hash is used because block itself is stored as pointer,
		// and when it reaches the miner - it's header is populated with timestamp and nonce,
		// that is leading ot the hash changes and failed comparison operations
		// even if original blocks are the same.
		newBlockHash := block.Header.BlockHash()
		if newBlockHash.IsEqual(&h.originalBitcoinBlockHash) {
			err = e.ErrDuplicatedBlock
			return
		}
	}

	h.bitcoin.block = block
	h.originalBitcoinBlockHash = block.Header.BlockHash()
	h.bitcoin.target = target
	h.bitcoin.height = height

	err = h.updateMergedMiningProof()
	return
}

func (h *Coordinator) updateTask() {
	if h.beacon == nil || (h.config.EnableBTCMining && h.bitcoin == nil) {
		// There is still no beacon chain block set.
		// Nothing to mine, even if some shards are initialized.
		return
	}

	newTask := &tasks.MinerTask{
		BeaconBlock:       h.beacon.block.Copy(),
		BeaconTarget:      h.beacon.target,
		BeaconBlockHeight: h.beacon.height,
	}

	if h.config.EnableBTCMining {
		newTask.BitcoinBlock = utils.BitcoinBlockCopy(h.bitcoin.block)
		newTask.BitcoinBlockHeight = h.bitcoin.height
		newTask.BitcoinBlockVersion = h.bitcoin.block.Header.Version
		newTask.BitcoinBlockBits = h.bitcoin.block.Header.Bits

		newTask.BitcoinBlockTarget = h.bitcoin.target
		if h.config.Network.Bitcoin.Params == "simnet" {
			// this is special case for local testnet, to prevent super low difficulty at bitcoin
			newTask.BitcoinBlockBits = h.beacon.block.Header.Bits()
			newTask.BitcoinBlock.Header.Bits = h.beacon.block.Header.Bits()
			newTask.BitcoinBlockTarget = h.beacon.target
		}
	}

	for shardID, shard := range h.shards {
		shardTask := tasks.ShardTask{
			ID:             shardID,
			BlockCandidate: shard.block,
			BlockHeight:    shard.height,
			Target:         shard.target,
		}
		newTask.ShardsTargets = append(newTask.ShardsTargets, shardTask)
	}

	// Sort shards tasks in ascending order.
	// todo: [optimisation] https://gitlab.com/jaxnet/core/miner/-/issues/7
	sort.Slice(newTask.ShardsTargets, func(i, j int) bool {
		return newTask.ShardsTargets[i].Target.Cmp(newTask.ShardsTargets[j].Target) == -1
	})

	h.taskMutex.Lock()
	{
		h.task = newTask
	}
	h.taskMutex.Unlock()
}

func (h *Coordinator) updateMergedMiningProof() (err error) {
	knownShardsCount := len(h.config.Network.Shards)
	fetchedShardsCount := len(h.shards)

	if knownShardsCount == 0 || fetchedShardsCount == 0 {
		return
	}

	tree := mm.NewSparseMerkleTree(uint32(knownShardsCount))
	for id, shard := range h.shards {
		// Shard IDs are going to be indexed from 1,
		// but the tree expects slots to be indexed from 0.
		slotIndex := uint32(id - 1)

		shardBlockHash := shard.block.Header.(*wire.ShardHeader).ShardBlockHash()
		err = tree.SetShardHash(slotIndex, shardBlockHash)
		if err != nil {
			return
		}
	}

	root, err := tree.Root()
	if err != nil {
		return
	}

	rootHash, err := chainhash.NewHash(root[:])
	if err != nil {
		return
	}

	coding, codingBitLength, err := tree.CatalanNumbersCoding()
	if err != nil {
		return
	}

	hashes := tree.MarshalOrangeTreeLeafs()

	h.beacon.block.Header.BeaconHeader().SetMergeMiningRoot(*rootHash)
	h.beacon.block.Header.BeaconHeader().SetMergedMiningTreeCodingProof(hashes, coding, codingBitLength)
	return
}
