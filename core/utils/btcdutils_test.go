package utils

import (
	"testing"
)

func TestMerkleTreeBranchString(t *testing.T) {
	tests := []struct {
		data    []string
		merkles []string
		branch  []string
	}{
		{
			data:    []string{"0", "1"},
			merkles: []string{"0", "1", "01"},
			branch:  []string{"1"},
		},
		{
			data:    []string{"0", "1", "2", "3"},
			merkles: []string{"0", "1", "2", "3", "01", "23", "0123"},
			branch:  []string{"1", "23"},
		},
		{
			data:    []string{"0", "1", "2", "3", "4", "5", "6", "7"},
			merkles: []string{"0", "1", "2", "3", "4", "5", "6", "7", "01", "23", "0123", "45", "67", "4567", "01234567"},
			branch:  []string{"1", "23", "4567"},
		},
	}

	for _, test := range tests {
		// t.Logf("test data: %+v; merkles: %+v", test.data, test.merkles)
		got := MerkleTreeBranchString(test.merkles)
		// t.Logf("MerkleTreeBranchString: got: (%+v); want: (%+v)", got, test.branch)
		if !testEqStringSlice(got, test.branch) {

			t.Errorf("MerkleTreeBranchString: got: (%+v); want: (%+v)", got, test.branch)
			return
		}
	}
}

// helper function to test string slices for equality
func testEqStringSlice(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}
