package utils

import (
	"math"

	btcdchainhash "github.com/btcsuite/btcd/chaincfg/chainhash"
	btcdwire "github.com/btcsuite/btcd/wire"
	"gitlab.com/jaxnet/jaxnetd/types/chainhash"
	"gitlab.com/jaxnet/jaxnetd/types/wire"
)

// BitcoinBlockCopy creates a deep copy of a MsgBlock so that the original does not get
// modified when the copy is manipulated.
func BitcoinBlockCopy(msg *btcdwire.MsgBlock) *btcdwire.MsgBlock {
	clone := new(btcdwire.MsgBlock)
	clone.Header = msg.Header
	clone.Transactions = make([]*btcdwire.MsgTx, len(msg.Transactions))
	for i, tx := range msg.Transactions {
		clone.Transactions[i] = tx.Copy()
	}

	return clone
}

// nextPowerOfTwo returns the next highest power of two from a given number if
// it is not already a power of two.  This is a helper function used during the
// calculation of a merkle tree.
func nextPowerOfTwo(n int) int {
	// Return the number if it's already a power of 2.
	if n&(n-1) == 0 {
		return n
	}

	// Figure out and return the next power of two.
	exponent := uint(math.Log2(float64(n))) + 1
	return 1 << exponent // 2^exponent
}

func MerkleTreeBranch(merkles []*btcdchainhash.Hash) (path []*btcdchainhash.Hash) {
	b := len(merkles)

	for k := b; k > 1; {
		path = append(path, merkles[b-k+1])
		k = (k - 1) / 2
	}

	return
}

func MerkleTreeBranchString(merkles []string) (path []string) {
	b := len(merkles)

	for k := b; k > 1; {
		path = append(path, merkles[b-k+1])
		k = (k - 1) / 2
	}

	return
}

func BtcBlockToBlockAux(btcBlock *btcdwire.MsgBlock) wire.BTCBlockAux {
	btcCoinbaseTx := btcBlock.Transactions[0].Copy()
	coinbaseTx := BtcTxToJaxTx(btcCoinbaseTx)
	merkles := make([]chainhash.Hash, len(btcBlock.Transactions))

	for i, transaction := range btcBlock.Transactions {
		merkles[i] = chainhash.Hash(transaction.TxHash())
	}

	return wire.BTCBlockAux{
		Version:     btcBlock.Header.Version,
		PrevBlock:   chainhash.Hash(btcBlock.Header.PrevBlock),
		MerkleRoot:  chainhash.Hash(btcBlock.Header.MerkleRoot),
		Timestamp:   btcBlock.Header.Timestamp,
		Bits:        btcBlock.Header.Bits,
		Nonce:       btcBlock.Header.Nonce,
		CoinbaseAux: wire.CoinbaseAux{Tx: coinbaseTx, TxMerkle: merkles},
	}
}

func BtcTxToJaxTx(tx *btcdwire.MsgTx) wire.MsgTx {
	// tx := btcBlock.Transactions[0].Copy()
	msgTx := wire.MsgTx{
		Version:  tx.Version,
		TxIn:     make([]*wire.TxIn, len(tx.TxIn)),
		TxOut:    make([]*wire.TxOut, len(tx.TxOut)),
		LockTime: tx.LockTime,
	}

	for i := range msgTx.TxIn {
		msgTx.TxIn[i] = &wire.TxIn{
			PreviousOutPoint: wire.OutPoint{
				Hash:  chainhash.Hash(tx.TxIn[i].PreviousOutPoint.Hash),
				Index: tx.TxIn[i].PreviousOutPoint.Index,
			},
			SignatureScript: tx.TxIn[i].SignatureScript,
			Witness:         wire.TxWitness(tx.TxIn[i].Witness),
			Sequence:        tx.TxIn[i].Sequence,
		}
	}

	for i := range msgTx.TxOut {
		msgTx.TxOut[i] = &wire.TxOut{
			Value:    tx.TxOut[i].Value,
			PkScript: tx.TxOut[i].PkScript,
		}
	}
	return msgTx
}

func JaxTxToBtcTx(tx *wire.MsgTx) btcdwire.MsgTx {
	msgTx := btcdwire.MsgTx{
		Version:  tx.Version,
		TxIn:     make([]*btcdwire.TxIn, len(tx.TxIn)),
		TxOut:    make([]*btcdwire.TxOut, len(tx.TxOut)),
		LockTime: tx.LockTime,
	}

	for i := range msgTx.TxIn {
		msgTx.TxIn[i] = &btcdwire.TxIn{
			PreviousOutPoint: btcdwire.OutPoint{
				Hash:  btcdchainhash.Hash(tx.TxIn[i].PreviousOutPoint.Hash),
				Index: tx.TxIn[i].PreviousOutPoint.Index,
			},
			SignatureScript: tx.TxIn[i].SignatureScript,
			Witness:         btcdwire.TxWitness(tx.TxIn[i].Witness),
			Sequence:        tx.TxIn[i].Sequence,
		}
	}

	for i := range msgTx.TxOut {
		msgTx.TxOut[i] = &btcdwire.TxOut{
			Value:    tx.TxOut[i].Value,
			PkScript: tx.TxOut[i].PkScript,
		}
	}
	return msgTx
}
