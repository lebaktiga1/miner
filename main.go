/*
 * Copyright (c) 2020 The JaxNetwork developers
 * Use of this source code is governed by an ISC
 * license that can be found in the LICENSE file.
 */

package main

import (
	"sync"

	c "gitlab.com/jaxnet/core/miner/core/communicator"
	"gitlab.com/jaxnet/core/miner/core/e"
	"gitlab.com/jaxnet/core/miner/core/logger"
	m "gitlab.com/jaxnet/core/miner/core/miner"
	"gitlab.com/jaxnet/core/miner/core/settings"
	s "gitlab.com/jaxnet/core/miner/core/state"
	ss "gitlab.com/jaxnet/core/miner/core/stratum"
)

var (
	miner        *m.Miner
	coordinator  *s.Coordinator
	communicator *c.Communicator
	stratum      *ss.Server
)

func main() {
	logger.Init()

	observer, err := settings.NewObserver()
	e.AbortOn(err)

	go observer.ObserveConfigurationChanges()

	for {
		select {
		case conf := <-observer.ConfigurationsFlow():
			synchronouslyStopAllComponents()
			reinitialiseWithConfiguration(conf)
		}
	}
}

func reinitialiseWithConfiguration(conf *settings.Configuration) {
	miner = m.New(conf)
	coordinator = s.New(conf)
	communicator = c.New(conf)

	// If merge mining is disabled do not start stratum server
	if conf.EnableStratum {
		stratum = ss.NewStratumServer(conf)
		stratum.Init(miner.Results())

		go stratum.RunUsing(coordinator)
	} else {
		go miner.RunUsing(coordinator)
	}

	go coordinator.RunUsing(
		communicator.ShardsBlockCandidates(),
		communicator.BeaconBlockCandidates(),
		communicator.BitcoinBlockCandidates(),
	)

	go communicator.RunUsing(miner.Results())
}

func synchronouslyStopAllComponents() {
	group := &sync.WaitGroup{}

	if miner != nil {
		group.Add(1)
		miner.StopUsing(group)
	}

	if coordinator != nil {
		group.Add(1)
		communicator.StopUsing(group)
	}

	if communicator != nil {
		group.Add(1)
		communicator.StopUsing(group)
	}
}
